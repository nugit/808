<?php
// セッション開始
session_start();
 
// セッション変数に値を入れる
//$_SESSION['user'] = 'nagasawa';
 
// セッションの値を初期化
$_SESSION = array();
 
// セッションを破棄
session_destroy();
 
// 中身を確認
//var_dump($_SESSION['user'])
?>

<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
	    <br>
		ログアウトしました。<br><br>
		<div><a href="index.php">トップページへ</a></div>
		
	</body>
</html>