<?php
session_start();
// top.phpのformタグから送信された内容と$_SESSION['user_id']の中身を各変数へ格納する
$product_id = $_POST['product_id'];
$number = $_POST['number'];
$user_id = $_SESSION['user_id'];

// registered_nameを出力する方法
// $name = $_SESSION['registered_name'];
// print $name;
// $name = $_POST['registered_name'];
// print $name;
// $name = $_SESSION['user_id'];
// print $name;

    // $numberが数値ではない、又は1未満の時の処理。当初 !is_numeric を使用したが小数が通ってしまう為 !is_int にした。
    if(!is_int($number) && $number < 1) {
        print '入力エラーです。0以上の整数を入力して下さい。';
        print '<br>';
		print '<form>';
		print '<input type="button" onclick="history.back()" value="戻る">';
		print '</form>';
    } else {
    // $numberが1以上の整数の時の処理
        $dsn = 'mysql:host=localhost;dbname=808;charset=utf8mb4';
        $username = 'root';
        $password = 'root';
        $dbh = new PDO($dsn, $username, $password);
        $dbh->query('SET NAMES utf8');
        // 静的プレースホルダ(=プリペアドステートメント、バインド機構)を指定
        $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        // DBエラー発生時は例外を投げる設定
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        // その商品が既にカートに存在している場合、在庫数を変更する画面に誘導する。
        // ここでは直接関係ないがテーブルを結合する場合はJOINを使用する。
        $sql = 'SELECT * FROM cart_table WHERE user_id = :user_id AND product_id = :product';
		$stmt = $dbh->prepare($sql);
		$stmt->bindValue(':product', $product_id, PDO::PARAM_STR);
		$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);

		$stmt->execute();
		// 実行結果を配列に返し（fetchALLは該当するデータを全て、配列として返す）、変数$productsへ格納する。
		$products = $stmt->fetchAll();
		
    		// デバック用。配列$productsの中身のデータの個数を数えさせている。
    		// print count($products);
    		// $productsが存在する場合、DBには登録しないでエラーメッセージを出力する
    		if (count($products) !== 0){
    			print 'この商品は既にカートに入っています。';
    			print '<br>';
     			print '<br>';
    			print 'カート一覧ページで数量の変更をして下さい。';
     			print '<br>';
     			print '<br>';
     			print '<a href="cart.php">カート一覧ページへ</a>';
    		}else{
             // その商品がカートにない場合、カートに新規に追加を行う。
            $stmt = $dbh -> prepare("INSERT INTO cart_table (user_id, product_id, number) VALUES (:user_id, :product_id, :number)");
            $stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
            $stmt->bindValue(':product_id', $product_id, PDO::PARAM_INT);
            $stmt->bindValue(':number', $number, PDO::PARAM_INT);
            $stmt->execute();
    
            print 'ユーザーID' . $user_id . 'のお客様が';
            print '<br>';
            print '<br>';
            print '商品番号' . $product_id . 'の商品を';
            print '<br>';
            print '<br>';
            print $number . '個' .カートへ入れました。;
            print '<br>';
            print '<br>';
            print '<a href="top.php">トップページへ</a>';
            
        }
    }
?>

