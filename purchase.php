<?php
    session_start();
    $user_id = $_SESSION['user_id'];
    // データベースに接続し設定する
    $dsn = 'mysql:host=localhost;dbname=808;charset=utf8mb4';
    $username = 'root';
    $password = 'root';
    $dbh = new PDO($dsn, $username, $password);
    // 静的プレースホルダを指定
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	// DBエラー発生時は例外を投げる設定
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	// カート情報を全て取得
	$sql = 'SELECT * FROM cart_table WHERE user_id=:user_id';
	$stmt = $dbh -> prepare($sql);
	$stmt -> bindValue(':user_id', $user_id, PDO::PARAM_INT);
	$stmt -> execute();
	$select_data = $stmt->fetchAll();
	foreach($select_data as $value) {
	    // 購入数が在庫数より大きい場合はエラーメッセージ。購入数の変更を促す
	    // 購入数が在庫数以下の場合、続行。
	    // その商品の在庫数が幾つであるのかを求める。
	    $sql = 'SELECT * FROM product_table WHERE product_id=:product_id';
	    $stmt = $dbh -> prepare($sql);
    	$stmt -> bindValue(':product_id',$value['product_id'] , PDO::PARAM_INT);
    	$stmt -> execute();
    	$item_data = $stmt->fetchAll();
    	$product_stock = $item_data[0]['product_stock'];
    	print '商品番号' . $value['product_id'] . 'の在庫数は' . $product_stock . '個です。<br>';
    	print '商品番号' . $value['product_id'] . 'を' . $value['number'] . '個買おうとしております。<br>';
	    if($product_stock < $value['number']) {
	        print  '商品番号' . $value['product_id'] . 'が販売可能数量を上回っております。' . $product_stock . '以下の数量に変更をお願致します<br>';
	    }else{
	        $sql = 'UPDATE product_table SET product_stock=product_stock-:number WHERE product_id=:product_id';
    	    $stmt = $dbh -> prepare($sql);
        	$stmt -> bindValue(':number', $value['number'], PDO::PARAM_INT);
        	$stmt -> bindValue(':product_id', $value['product_id'], PDO::PARAM_INT);
        	$stmt -> execute();
        	
        	$sql = 'INSERT INTO purchase_table(user_id, product_id, number) VALUES (:user_id, :product_id, :number)';
    	    $stmt = $dbh -> prepare($sql);
        	$stmt -> bindValue(':user_id', $user_id, PDO::PARAM_INT);
        	$stmt -> bindValue(':product_id', $value['product_id'], PDO::PARAM_INT);
        	$stmt -> bindValue(':number', $value['number'], PDO::PARAM_INT);
        	$stmt -> execute();
    	    //print $value['product_id'];
    	    //print $value['number'];
    	    // カートテーブルからカート情報を消す
            $sql = "DELETE FROM cart_table WHERE user_id=:user_id AND product_id=:product_id";
        	$stmt = $dbh -> prepare($sql);
        	$stmt -> bindValue(':user_id', $user_id, PDO::PARAM_INT);
        	$stmt -> bindValue(':product_id', $value['product_id'], PDO::PARAM_INT);

        	$stmt -> execute();
        	print '商品番号' . $value['product_id'] . 'を' . $value['number'] . '個購入しました。<br>';
	    }
	    
    	
	}
    
    
?>


<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <title>商品購入</title>
    </head>
    <body>
        
        
        <div><a href="top.php">トップへ</a></div>
        
    </body>
</html>

