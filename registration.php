<!--重複チェック機能が未だできていない-->
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<title>新規会員登録</title>
	</head>
	<body>
		<h1>新規会員登録</h1>
		<p>お名前、登録メールアドレス、パスワード、ご住所を全てご入力の上、登録ボタンを押して下さい。</p>
		<form method="post" action="check.php" enctype="multipart/form-data">
			お名前
			<br>
			<div><input type="text" name="registered_name"></div>
			<br>
			登録メールアドレス
			<br>
			<div><input type="email" name="registered_email"></div>
			<br>
			パスワード
			<br>
			<div><input type="password" name="registered_password"></div>
			<br>
			ご住所
			<br>
			<div><input type="text" name="registered_address"></div>
			<br>
			<div><input type="submit" value="登録"></div>
			<br>
		</form>
		
			
		<div><a href="top.php">トップページへ戻る</a></div>
		
	</body>
</html>