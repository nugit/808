<?php
    //セッション
    session_start();
    $user_id = $_SESSION['user_id'];
    // デバック用 
    // print 'user_id=' . $user_id;
    $img_dir = './images/'; 

	$dsn = 'mysql:host=localhost;dbname=808;charset=utf8mb4';
    $username = 'root';
    $password = 'root';
    $dbh = new PDO($dsn, $username, $password);
    // 静的プレースホルダ(=プリペアドステートメント,バインド機構)を指定
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	// DBエラー発生時は例外を投げる設定
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// クラスPDOのオブジェクト$dbhに、引数がSQL文（product_tableの全てを選択という内容）のqueryメソッドを呼び出し変数$stmtに格納する
	$stmt = $dbh->prepare('SELECT * FROM purchase_table WHERE user_id = :user_id');
	$stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
	// $stmt(中身はオブジェクト$dbh)にexecuteメソッドを実行する
	$stmt->execute();
    // 実行結果を配列に返し（fetchALLは該当するデータを全て、配列として返す）、変数$select_dataへ格納する。
    $select_data = $stmt->fetchAll();
    
    // クラスPDOのオブジェクト$dbhに、引数がSQL文（registered_id=:user_idとなるregistered_tableのカラムregistered_nameを含むレコードを抽出する）のprepareメソッドを呼び出し変数$stmtに格納する
    $stmt = $dbh -> prepare("SELECT registered_name FROM registered_table WHERE registered_id=:user_id");
    // $stmt(中身はオブジェクト$dbh)にbindValueメソッドを実行する（:user_idに$user_idを数値としてバインドする）
	$stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
	// $stmt(中身はオブジェクト$dbh)にexecuteメソッドを実行する
	$stmt->execute();
	// $stmt(中身はオブジェクト$dbh)にfetchColumnメソッド（引数で指定した番号のカラムを文字列で取得する。省略時は０を指定したとみなす。）を実行する。この$user_nameが○○様いらっしゃいませの○○になる。
	$user_name = $stmt->fetchColumn(0);
	
	// 29行目の$stmtの中身は23行目の$stmtで23行目のprepare以下のの条件を満たすレコードが配列として格納されている。
	// しかし23行目のprepare以下のの条件を満たすレコードは一つしかあり得ない為、要素が一つの配列にしかなり得ない。
	// その為、fetchColumn(0)でDBのregistered_nameが得られる。
?>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>808−3 ネット専門の八百屋さん♫</title>
	</head>
	<body class="clearfix">
		<header class="clearfix">
			<h1>８０８−３（やおやさん）へようこそ！</h1>
			<!--○○様 いらっしゃいませ、を表示する為にPHP挿入-->
			<h3>
				<?php
					print $user_name . "  様の購入履歴となります。";
				?>			
			</h3>
			<nav class="navigation clearfix">
				<ul>
					<li><a href="cart.php">カートへ</a></li>
					<li><a href="purchase_history.php">購入履歴へ</a></li>
					<li><a href="logout.php">ログアウトする</a></li>
				</ul>
			</nav>
		</header>
			
		<main class="clearfix" >
                <!-- 配列$select_data内の商品を表示する -->
                <?php foreach($select_data as $data){ ?>
                    <ul>
                        <li>
                            <?php  print $data['purchase_id']; ?>
                        </li>
                        <li>
                            <?php  print $data['user_id']; ?>
                        </li>
                        <li>
                            <?php  print $data['product_id']; ?>
                        </li>
                        <li>
                            <?php  print $data['number']; ?>
                        </li>
                        <li>
                            <?php  print $data['modified']; ?>
                        </li>
                    </ul>
                <?php } ?>
            </div>
        </main>
        <footer>
			<p class="pagetop"><a href="#top">ページの先頭へ</a></p>
			<p class="copyright"><small>&copy; Copyright 2018 株式会社八百屋さん</small></p>
		</footer>
	</body>
</html>