<?php
    // セッション開始
    session_start();
    // セッションからユーザIDを引き出す
	$user_id = $_SESSION['user_id'];
	// cart.phpからポスト通信（hiddenで）で送られてきた商品番号を取得
	$product_id = $_POST['product_id'];
	// cart.phpからポスト通信（hiddenではなく）で送られてきた変更後の個数を取得
	$number = $_POST['number'];
	print $product_id . 'の商品を' . $number . '個に変更しました。';
	
	// 13〜20はDBへ接続する決まり文句
	$dsn = 'mysql:host=localhost;dbname=808;charset=utf8mb4';
    $username = 'root';
    $password = 'root';
    $dbh = new PDO($dsn, $username, $password);
    // 静的プレースホルダを指定
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	// DBエラー発生時は例外を投げる設定
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // カートテーブルから商品の個数を変更する。
	$sql = "UPDATE cart_table SET number=:number WHERE user_id=:user_id AND product_id=:product_id";
	$stmt = $dbh -> prepare($sql);
	$stmt -> bindValue(':number', $number, PDO::PARAM_INT);
	$stmt -> bindValue(':user_id', $user_id, PDO::PARAM_INT);
	$stmt -> bindValue(':product_id', $product_id, PDO::PARAM_INT);
	$stmt -> execute();
?>
<a href = "cart.php">カート一覧へ</a>