<?php
    //セッション
    session_start();
    $user_id = $_SESSION['user_id'];
    // デバック用 
    // print 'user_id=' . $user_id;
    $img_dir = './images/'; 

	$dsn = 'mysql:host=localhost;dbname=808;charset=utf8mb4';
    $username = 'root';
    $password = 'root';
    $dbh = new PDO($dsn, $username, $password);
    // 静的プレースホルダ(=プリペアドステートメント,バインド機構)を指定
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	// DBエラー発生時は例外を投げる設定
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// クラスPDOのオブジェクト$dbhに、引数がSQL文（product_tableの全てを選択という内容）のqueryメソッドを呼び出し変数$stmtに格納する
	$stmt = $dbh->query('SELECT * FROM product_table');
    // 実行結果を配列に返し（fetchALLは該当するデータを全て、配列として返す）、変数$select_dataへ格納する。
    $select_data = $stmt->fetchAll();
    
    // クラスPDOのオブジェクト$dbhに、引数がSQL文（registered_id=:user_idとなるregistered_tableのカラムregistered_nameを含むレコードを抽出する）のprepareメソッドを呼び出し変数$stmtに格納する
    $stmt = $dbh -> prepare("SELECT registered_name FROM registered_table WHERE registered_id=:user_id");
    // $stmt(中身はオブジェクト$dbh)にbindValueメソッドを実行する（:user_idに$user_idを数値としてバインドする）
	$stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
	// $stmt(中身はオブジェクト$dbh)にexecuteメソッドを実行する
	$stmt->execute();
	// $stmt(中身はオブジェクト$dbh)にfetchColumnメソッド（引数で指定した番号のカラムを文字列で取得する。省略時は０を指定したとみなす。）を実行する。この$user_nameが○○様いらっしゃいませの○○になる。
	$user_name = $stmt->fetchColumn(0);
	
	// 29行目の$stmtの中身は23行目の$stmtで23行目のprepare以下のの条件を満たすレコードが配列として格納されている。
	// しかし23行目のprepare以下のの条件を満たすレコードは一つしかあり得ない為、要素が一つの配列にしかなり得ない。
	// その為、fetchColumn(0)でDBのregistered_nameが得られる。
?>

<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>808−3 ネット専門の八百屋さん♫</title>
		<meta name="description" content="808-3は千葉県産の新鮮な野菜をネット専門で販売しているオンラインストアです。ご自宅周辺にスーパー等がない方にも新鮮で美味しい野菜や果物をお届けします。">
		<link rel="stylesheet" href="css/index.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="jquery/jquery.bxslider.min.js"></script>
		<link href="jquery/jquery.bxslider.css" rel="stylesheet" />
		<script type="text/javascript">
			$(document).ready(function(){
				$('.bxslider').bxSlider({
					auto: true,
					mode: 'fade',
					pause: 5000,
				});
			});
		</script>
	</head>
	<body class="clearfix">
		<header class="clearfix">
			<h1>８０８−３（やおやさん）へようこそ！</h1>
			<!--○○様 いらっしゃいませ、を表示する為にPHP挿入-->
			<h3>
				<?php
					print $user_name . "  様、いらっしゃいませ";
				?>			
			</h3>
			<nav class="navigation clearfix">
				<ul>
					<li><a href="cart.php">カートへ</a></li>
					<li><a href="purchase_history.php">購入履歴へ</a></li>
					<li><a href="logout.php">ログアウトする</a></li>
				</ul>
			</nav>
		</header>
			
		<main class="clearfix" >
			<div class="mainvisual">
				<ul class="bxslider">
					<li><img src="images/main.jpg" alt="市場の風景"></li>
					<li><img src="images/main2.jpg" alt="チーバくん"></li>
					<li><img src="images/main3.png" alt="野菜一覧"></li>
				</ul>
			</div>
                <!-- 配列$select_data内の商品を表示する -->
                <?php foreach($select_data as $data){ ?>
                    <section class="vege">
                        <img src="<?php print $img_dir . $data['product_img']; ?>">
                        <h4><?php print $data['product_name']; ?></h4>
                        <p>1個 <?php print $data['product_price']; ?>円</p>
                        <p>個数</p>
                        <form method="post" action="cart_in.php">
                            <input type="text" name="number">
                            <input type="hidden" name="product_id" value="<?php print $data['product_id']; ?>">
                            <input type="submit" value="カートに入れる">
                        </form>
                    </section>
                <?php } ?>
            </div>
        </main>
        
<script>
  src="http://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous">
</script>
	
	
		<footer>
			<p class="pagetop"><a href="#top">ページの先頭へ</a></p>
			<p class="copyright"><small>&copy; Copyright 2018 株式会社八百屋さん</small></p>
		</footer>
	</body>
</html>