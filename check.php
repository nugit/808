<!-- 新規会員登録の入力漏れ等をチェックするページ -->
<!--重複チェックが未だできていない-->
<?php
	// registraiton.phpのformタグ内から飛んできたname属性を、配列である$_POSTにkeyとして格納し、それを$nameに代入する。以下同様。
	$name = $_POST['registered_name'];
	print "お名前";
	print '<br>';
	print $name . " 様";
	print '<br>';
	print '<br>';
	$mail = $_POST['registered_email'];
	print "登録メールアドレス";
	print '<br>';
	print $mail;
	print '<br>';
	print '<br>';
	$password = $_POST['registered_password'];
	print "パスワード";
	print '<br>';
	print $password;
	print '<br>';
	print '<br>';
	$address = $_POST['registered_address'];
	print "ご住所";
	print '<br>';
	print $address;
	print '<br>';
	print '<br>';

	
	
	
	//入力項目に一つでも空欄があった時の処理
    if($name === '' || $mail === '' || $password === '' || $address === '') {
		print "入力エラーです。入力項目を全て入力して下さい。";
		print '<form>';
		print '<input type="button" onclick="history.back()" value="戻る">';
		print '</form>';
	} else {
	//入力項目が全て埋められていた時の処理
		session_start();
		// $dsn はData Source Name から作った変数名
		$dsn = 'mysql:dbname=808;host=localhost';
		$user = 'root';
		$user_password = 'root';
		// $dbh はData Base Handler から作った変数名
		$dbh = new PDO($dsn, $user, $user_password);
		// MySQLのDBに情報を格納する時の文字化けを解消できる命令文
		$dbh->query('SET NAMES utf8');
		
		// このメールアドレスを既に登録されている人がいるのかチェックする
		// されていなければ新規登録を許可する
		// されている場合は、このメールアドレスは既に使用されていますと表示してDBには登録しない
		$sql = 'SELECT * FROM registered_table WHERE registered_email = :email';
		$stmt = $dbh->prepare($sql);
		$stmt->bindValue(':email', $mail, PDO::PARAM_STR);
		$stmt->execute();
		// 実行結果を配列に返し（fetchALLは該当するデータを全て、配列として返す）、変数$usersへ格納する。
		$users = $stmt->fetchAll();
		// デバック用。配列$usersの中身のデータの個数を数えさせている。
		//	print count($users);
		// $usersが存在する場合、DBには登録しないでエラーメッセージを出力する
		if (count($users) !== 0){
			print 'このメールアドレスは既に登録済です。';
			print '<br>';
			print '<br>';
			print '<a href="registration.php">新規会員登録ページへ</a>';
		}else{
			$sql = 'INSERT INTO registered_table (registered_name, registered_email, registered_password, registered_address) VALUES ("'.$name.'","'.$mail.'","'.$password.'","'.$address.'")'; 
			// クラスPDOのオブジェクト$dbhに、引数$sqlのprepareメソッドを呼び出し、変数$stmtに格納する。
			$stmt = $dbh->prepare($sql);
			// $stmt(中身はオブジェクト$dbh)にexecuteメソッドを実行する。
			$stmt->execute();
			// 追加されたregistered_id(これが追加されたデータのIDに当たる)を求める。
			// 以下は新規会員登録直後のログインしたページで○○様いらっしゃいませを表示させるためのセッション
			// クラスPDOのオブジェクト$dbhにデータを登録後にlaseInsertIdメソッド
			// (PDOに元々用意されているメソッド。データ登録後にこのメソッドを使うとそのデータのIDを取得する)
			// を使いそのデータのIDを求めて変数$user_idに代入する。
			$user_id = $dbh->lastInsertId();
			// デバック用
			// print 'user_id=' . $user_id;
			// 3行上の$user_idを'user_id'というキーに紐付けてセッションに保存する
			$_SESSION['user_id'] = $user_id;
			// 実行が終わったのでオブジェクト$dbhを空にする。
			$dbh = null;
			print '<br>';
			print 'データの登録を完了しました。';
			print '<br>';
			print '<a href="top.php">トップページへ</a>';
		}
	
	}
?>