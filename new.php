<?php
// アップロードした画像を保存するフォルダを指定
$img_dir    = './images/'; 
// 下の2行は変数の初期化と呼ばれる形式的なもの
$new_img_filename = '';   // アップロードした新しい画像ファイル名
$err_msg = array(); 
 
// POST通信が行われたならば
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	// 商品名が入力されていたら
	if(isset($_POST['product_name']) === TRUE && $_POST['product_name'] !== ''){
		    $product_name = $_POST['product_name'];
	} else {
		$err_msg[] = '商品名を入力して下さい。';
	}
	// 商品の価格が入力されていたら
	// is_numeric($_POST['product_price']) && $_POST['product_price'] > 1 でエラーチェック→is_numericをis_intへ変更した。
	if(isset($_POST['product_price']) === TRUE && $_POST['product_price'] !== '' && is_int($_POST['product_price']) && $_POST['product_price'] > 1){
		    $product_price = $_POST['product_price'];
	} else {
		$err_msg[] = '商品の価格を0以上の整数で入力して下さい。';
	}
	// 商品の在庫数が入力されてたら→is_numericをis_intへ変更した。
	if(isset($_POST['product_stock']) === TRUE && $_POST['product_stock'] !== '' && is_int($_POST['product_stock']) && $_POST['product_stock'] > 1){
		    $product_stock = $_POST['product_stock'];
	} else {
		$err_msg[] = '商品の在庫数を0以上の整数で入力して下さい。';
	}
    
    // HTTP POST でファイルがアップロードされたかどうかチェック
	// 14~35 決まり文句
	if (is_uploaded_file($_FILES['product_img']['tmp_name']) === TRUE) {
		// 画像の拡張子を取得
		$extension = pathinfo($_FILES['product_img']['name'], PATHINFO_EXTENSION);
		// 指定の拡張子であるかどうかチェック
		if ($extension === 'jpg' || $extension === 'jpeg' || $extension === 'png') {
			// 保存する新しいファイル名の生成（ユニークな値を設定する）
			$new_product_img = sha1(uniqid(mt_rand(), true)). '.' . $extension;
			// 同名ファイルが存在するかどうかチェック
			if (is_file($img_dir . $new_product_img) !== TRUE) {
				// アップロードされたファイルを指定ディレクトリに移動して保存
				if (move_uploaded_file($_FILES['product_img']['tmp_name'], $img_dir . $new_product_img) !== TRUE) {
					$err_msg[] = 'ファイルアップロードに失敗しました';
				}
			} else {
				$err_msg[] = 'ファイルアップロードに失敗しました。再度お試しください。';
			}
		} else {
			$err_msg[] = 'ファイル形式が異なります。画像ファイルはJPEGもしくはPNGのみ利用可能です。';
		}
	} else {
		$err_msg[] = 'ファイルを選択してください';
	}
	// 入力エラーが一つも無ければデータベースへ接続する。→ この流れは書けなくてもおぼえておく
	// 以下は決まり文句で作ったものに合わせて必要なところだけ書き換える。
	if(count($err_msg) === 0){
		$dsn = 'mysql:host=localhost;dbname=808;charset=utf8mb4';
		$username = 'root';
		$password = 'root';
		$dbh = new PDO($dsn, $username, $password);
		$dbh->query('SET NAMES utf8');
		// 静的プレースホルダを指定
		$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		// DBエラー発生時は例外を投げる設定
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $dbh -> prepare("INSERT INTO product_table (product_name, product_price, product_stock, product_img) VALUES (:name, :price, :stock, :img)");
		$stmt->bindParam(':name', $product_name, PDO::PARAM_STR);
		$stmt->bindValue(':price', $product_price, PDO::PARAM_INT);
		$stmt->bindValue(':stock', $product_stock, PDO::PARAM_INT);
		$stmt->bindParam(':img', $new_product_img, PDO::PARAM_STR);
		$stmt->execute();
	}
	
} 
 
?>


<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <title>新規商品登録</title>
    </head>
    <body>
		<!-- new.php でエラーがあった数だけエラーの内容を繰り返し表示する -->
		<?php foreach($err_msg as $error){ ?>
			<p><?php print $error; ?></p>
		<?php } ?>
        <h1>新規商品のアップロード</h1>
		<!-- enctype="multipart/form-data" はサーバーにファイルを送信する時の決まり文句 -->
		<!-- action が指定されていない場合そのファイルの1行目に飛ぶ	-->
        <form method="post" enctype="multipart/form-data">
			商品名<br>
            <div><input type="text" name="product_name"></div><br>
			価格<br>
            <div><input type="text" name="product_price"></div><br>
            在庫数<br>
            <div><input type="text" name="product_stock"></div><br>
			画像<br>
			<!-- type="file" にしてサーバにファイルを送信 -->
            <div><input type="file" name="product_img"></div><br>
            
            <div><input type="submit" value="登録"></div>
        </form>
        
        <div><a href="index.php">トップへ</a></div>
     </body>
</html>
