<?php
	$cart_info = array();
	$img_dir = 'images/';
	session_start();
	$user_id = $_SESSION['user_id'];
	$sql = 'SELECT product_table.product_id, product_table.product_img, product_table.product_name, product_table.product_price, cart_table.number
			FROM cart_table JOIN product_table ON cart_table.product_id = product_table.product_id
			WHERE cart_table.user_id = :user_id ORDER BY cart_table.cart_id
			';
	
	$dsn = 'mysql:host=localhost;dbname=808;charset=utf8mb4';
	$username = 'root';
	$password = 'root';
	$dbh = new PDO($dsn, $username, $password);
	$dbh->query('SET NAMES utf8');
	// 静的プレースホルダを指定
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	// DBエラー発生時は例外を投げる設定
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt = $dbh -> prepare($sql);
	$stmt -> bindValue(':user_id', $user_id, PDO::PARAM_INT);
	$stmt -> execute();
	$cart_info = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$stmt = $dbh -> prepare("SELECT registered_name FROM registered_table WHERE registered_id=:user_id");
	$stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
	$stmt->execute();
	$user_name = $stmt->fetchColumn(0);

?>


<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<title>カート一覧</title>
		<style>
			table,tr,td,th{
				border: solid 1px;
			}
			img{
				width: 200px;
			}
			table{
				border-collapse: collapse;
			}
		</style>
	</head>
	<body>
		<h1><?php print $user_name; ?> 様のカート一覧</h1>
		<table>
			<tr>
				<th>商品画像</th>
				<th>商品名</th>
				<th>価格</th>
				<th>数量</th>
				<th>小計</th>
				<th>削除</th>
			</tr>
			<?php foreach($cart_info as $value){ ?>
			<tr>
				<td><img src = "<?php print $img_dir . $value['product_img']; ?>"></td>
				<td><?php print $value['product_name']; ?></td>
				<td><?php print $value['product_price']; ?>円</td>
				<td>
					<form method = "post" action ="cart_update.php">
						<input type = "text" name = "number" value = "<?php print $value['number']; ?>">個
						<input type = "hidden" name = "product_id" value = "<?php print $value['product_id']; ?>">
						<input type = "submit" value = "変更">
					</form>
				<!--小計を計算する-->
				<td><?php print $value['product_price'] * $value['number']; ?>円</td>
				<!--小計から合計を計算する下準備-->
				<?php
					// カートの中の全ての商品の小計の合算値をforeachが回る度に変数$total_priceに格納しておく
					$total_price[] = $value['product_price'] * $value['number'];
					// $total_price[]はカートの行数と同じ個数の小計の配列になりその合計をarray_sumで算出し変数$goukeiに格納しておく
					$goukei = array_sum($total_price);
				?>
				<td>
					<form method = "post" action = "cart_delete.php" >
						<input type = "submit" value = "削除">
						<input type = "hidden" name = "product_id" value = "<?php print $value['product_id']; ?>">
					</form>	
				</td>
			</tr>
			<?php } ?>
		</table>
		<table>
			<tr>
				<td><!--合計金額を計算して請求金額として表示する-->
				<?php
					$tax = 1.08;
					print 'ご請求金額(税込): ' . ceil($goukei * $tax) .' 円';
				?>
				</td>
			</tr>
		</table>
		
		<form method = "post" action = "purchase.php" >
			<input type = "submit" value = "購入">
		</form>
		<a href="top.php">トップページへ</a>
	</body>
</html>