<?php
	session_start();
	// アップロードした画像を保存するフォルダを指定
    $img_dir    = './images/'; 

	$dsn = 'mysql:host=localhost;dbname=808;charset=utf8mb4';
    $username = 'root';
    $password = 'root';
    $dbh = new PDO($dsn, $username, $password);
    // 静的プレースホルダを指定
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	// DBエラー発生時は例外を投げる設定
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	
	$smt = $dbh->query('SELECT * FROM product_table');
    // 実行結果を配列に返す。
    $select_data = $smt->fetchAll();
	
?>

<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>808−3 ネット専門の八百屋さん♫</title>
		<meta name="description" content="808-3は千葉県産の新鮮な野菜をネット専門で販売しているオンラインストアです。ご自宅周辺にスーパー等がない方にも新鮮で美味しい野菜や果物をお届けします。">
		<link rel="stylesheet" href="css/index.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="jquery/jquery.bxslider.min.js"></script>
		<link href="jquery/jquery.bxslider.css" rel="stylesheet" />
		<script type="text/javascript">
			$(document).ready(function(){
				$('.bxslider').bxSlider({
					auto: true,
					mode: 'fade',
					pause: 5000,
				});
			});
		</script>
	</head>

	<body class="clearfix">
		<header class="clearfix">
			<h1>８０８−３（やおやさん）へようこそ！</h1>
			<nav class="navigation clearfix">
				<ul>
					<li><a href="registration.php">会員登録</a></li>
					<li><a href="login.html">ログイン</a></li>
				</ul>
			</nav>
		</header>
			
		<main class="clearfix" >
			<div class="mainvisual">
				<ul class="bxslider">
					<li><img src="images/main.jpg" alt="市場の風景"></li>
					<li><img src="images/main2.jpg" alt="チーバくん"></li>
					<li><img src="images/main3.png" alt="野菜一覧"></li>
				</ul>
			</div>
   			<div>
   				<!-- 配列$select_data内の商品を表示する -->
                <?php foreach($select_data as $data){ ?>
                    <section class="vege">
                        <img src="<?php print $img_dir . $data['product_img']; ?>">
                        <h4><?php print $data['product_name']; ?></h4>
                        <p>1個 <?php print $data['product_price']; ?>円</p>
                    </section>
                <?php } ?>
            </div>
        </main>
		

<script>
  src="http://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous">
</script>
		<footer>
			<p class="pagetop"><a href="#top">ページの先頭へ</a></p>
			<p class="copyright"><small>&copy; Copyright 2018 株式会社八百屋さん</small></p>
		</footer>
	</body>
</html>