<?php
    // セッション開始
    session_start();
    // セッションからユーザIDを引き出す
	$user_id = $_SESSION['user_id'];
	// cart.phpからポスト通信（hiddenで）で送られてきた商品番号を取得
	$product_id = $_POST['product_id'];
	print '商品番号が' . $product_id . 'の商品を削除しました。';
	
	// 11〜18はDBへ接続する決まり文句
	$dsn = 'mysql:host=localhost;dbname=808;charset=utf8mb4';
    $username = 'root';
    $password = 'root';
    $dbh = new PDO($dsn, $username, $password);
    // 静的プレースホルダを指定
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	// DBエラー発生時は例外を投げる設定
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // カートテーブルから削除ボタンされた商品を削除する。
	$sql = "DELETE FROM cart_table WHERE user_id=:user_id AND product_id=:product_id";
	$stmt = $dbh -> prepare($sql);
	$stmt -> bindValue(':user_id', $user_id, PDO::PARAM_INT);
	$stmt -> bindValue(':product_id', $product_id, PDO::PARAM_INT);
	$stmt -> execute();
?>

<br>
<br>
<a href = "cart.php">カート一覧へ</a>